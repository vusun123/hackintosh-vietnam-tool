##Hackintosh Vietnam Tool 1.x

Author: Pham Thanh Hoang **danght**.

Maintainer: Dinh Hai Nguyen **pokenguyen**.


###Change log:

**Version 1.4 (February 21 2014):**

- Downgrade Intel ethernet kext to 2.5.4f
- Added Core 2 Duo, Core i 1st Gen clover config
- Added Intel Iris config
- Added AHCI kext

**Version 1.3 (February 16 2014):**

- Updated Elan, ALPS, Intel ethernet kexts
- Updated SSDTPrgen 10.5
- Added Core 2 Duo, Core i 1st Gen smbios

**Version 1.2 (February 11 2014):**

- Updated kexts
- Updated SSDTPrgen 10.0
- Added HD4600 desktop config
- Added Fixes section

**Version 1.0 (February 6 2014):**

- Initial Release



















###C r e d i t s:

**Chameleon Trunk** is courtesy of the **Chameleon Team**.  
[The Chameleon Trunk support thread](http://www.insanelymac.com/forum/topic/231075-chameleon-22svn-official-pkg-installer-binaries)  

The **Synaptics Touchpad** fork is courtesy of **RehabMan**  
[The Synaptics Touchpad support thread](http://www.tonymacx86.com/hp-probook/75649-new-voodoops2controller-keyboard-trackpad.html)  

The **Elan Touchpad** fork is courtesy of **EMlyDinEsH**  
[The Elan Touchpad support thread](http://forum.osxlatitude.com/index.php?/topic/1948-elan-touchpad-driver-mac-os-x/)  

The **ALPS Touchpad** fork is courtesy of **sontrg **, **bpedman** and **RehabMan**  
[The ALPS Touchpad support thread](http://forum.osxlatitude.com/index.php?/topic/2545-new-touchpad-driver-for-e6520-alps/)  

The **AppleACPIBattery** fork is courtesy of **RehabMan**  
[The AppleSmartBatteryManager support thread](http://www.tonymacx86.com/mountain-lion-laptop-support/69472-battery-manager-fix-boot-without-batteries.html)  
[The original support thread](http://www.insanelymac.com/forum/topic/264597-hp-dvx-acpi-3x4x-battery-driver-106107/#entry1729132)  

The SSDT generator script is courtesy of **RevoGirl** and **PikeRAlpha**.  
[The SSDT generator support thread](http://www.tonymacx86.com/ssdt/86906-ssdt-generation-script-ivybridge-pm.html)  

**MaciASL** is courtesy of **SJ_UnderWater**.  
[The MaciASL support thread](http://www.tonymacx86.com/dsdt/83565-native-dsdt-aml-ide-compiler-maciasl-open-beta.html)  

**GenericUSBXHCI.kext** is courtesy of **Zenith432** and **RehabMan**.  
**RealtekRTL8111.kext** is courtesy of **Mieze** and **RehabMan**.  
[The main support thread](http://www.tonymacx86.com/hp-probook/93732-new-kexts-proposed-probook-installer-v6-1-a.html)  
[The original GenericUSBXHCI.kext support thread](http://www.insanelymac.com/forum/topic/286860-genericusbxhci-usb-30-driver-for-os-x-with-source/)  
[The original RealtekRTL8111.kext support thread](http://www.insanelymac.com/forum/topic/287161-new-driver-for-realtek-rtl8111/)  

**AppleIntelE1000e.kext** is courtesy of **cVaD**.
[The original AppleIntelE1000e.kext support thread](http://www.insanelymac.com/forum/topic/205771-appleintele1000ekext-for-108107106105/)  

**Kext Utility** is courtesy of **hnak**.
[The original Kext Utility support thread](http://www.insanelymac.com/forum/topic/140647-latest-kext-utility-mavericks-super-speed-edition/)  

The AHCI patch, **patch-hda.pl** and **patch-hda-codecs.pl** are courtesy of **bcc9**.  
[The AHCI patch support thread](http://www.insanelymac.com/forum/topic/280062-waiting-for-root-device-when-kernel-cache-used-only-with-some-disks-fix/)  
[The AppleHDA patch support thread](http://www.insanelymac.com/forum/topic/284004-script-to-patch-applehda-binary-for-osx107108/)  

**Chameleon Wizard** and **Kext Wizard** are courtesy of **janek202**.  
[The Chameleon Wizard support thread](http://www.insanelymac.com/forum/topic/257464-chameleon-wizard-utility-for-chameleon/)  
[The Kext Wizard support thread](http://www.insanelymac.com/forum/topic/253395-kext-wizard-easy-to-use-kext-installer-and-more/)  

**FakeSMC** is courtesy of **RehabMan**, **kozlek**, **netkas**, **slice** and **navi**.  
[The main support thread](http://www.tonymacx86.com/hp-probook/)  
[The original support thread](http://www.insanelymac.com/forum/topic/275429-hwsensors/)  

**Trim Enabler** is courtesy of **Cindori**.  
[The Trim Enabler support forums](http://www.groths.org/forums/)  

**AICPMPatch.pl** is courtesy of **el coniglio** with modifications by **RehabMan**.  
[The original support thread](http://olarila.com/forum/viewtopic.php?f=9&t=1003&sid=d6df188c360c6a74d9b788ae9568df84)  

**FileNVRAM** copyright © 2013 **xZeneu LLC**. **FileNVRAM** is licensed under the Attribution-NonCommercial 3.0 Unported license (included in the Installer).  
[The FIleNVRAM support thread](http://www.insanelymac.com/forum/topic/286563-filenvram-113-released/)  

The unified **AppleIntelFramebufferCapri.kext** patch is courtesy of **kpkp**.  
[The original support thread](http://www.tonymacx86.com/hp-probook/99533-testers-ivy-probooks-needed.html)  

The Atheros Bluetooth kext is courtesy of **mac4mat** and **RehabMan**.  
The **patchmatic** utility by **RehabMan** is based of the **MaciASL** source code and uses **RegexKitLite** by **John Engelhart**.  
The **DSDT generator/patcher** is courtesy of **RehabMan**, **BigDonkey** and **philip_petev**.  

###Special thanks to:

**BlueKing** - the original creator of HP ProBook Installer.  
**Tegezee** - the second maintainer of HP ProBook Installer.  
**philippetev** - the current maintainer of HP ProBook Installer.  
**philippetev** - the current maintainer of Laptop DSDT Patch.  

###Thanks to all testers:

**Nope**  

###and the users of the Hackintosh - We Love Mac Facebook group.  

All rights reserved.  
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:  
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.  
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.  
Neither the name of the Zang Industries nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.  
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  